<?php

namespace Drupal\wt_chunks\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Chunk type configuration entity.
 *
 * @ConfigEntityType(
 *   id = "chunk_type",
 *   label = @Translation("Chunk type"),
 *   handlers = {
 *     "form" = {
 *       "add" = "Drupal\wt_chunks\Form\ChunkTypeForm",
 *       "edit" = "Drupal\wt_chunks\Form\ChunkTypeForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *     "list_builder" = "Drupal\wt_chunks\ChunkTypeListBuilder",
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   admin_permission = "administer chunk types",
 *   bundle_of = "chunk",
 *   config_prefix = "chunk_type",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "add-form" = "/admin/structure/chunk_types/add",
 *     "edit-form" = "/admin/structure/chunk_types/manage/{chunk_type}",
 *     "delete-form" = "/admin/structure/chunk_types/manage/{chunk_type}/delete",
 *     "collection" = "/admin/structure/chunk_types"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "uuid",
 *   }
 * )
 */
class ChunkType extends ConfigEntityBundleBase {

  /**
   * The machine name of this chunk type.
   *
   * @var string
   */
  protected $id;

  /**
   * The human-readable name of the chunk type.
   *
   * @var string
   */
  protected $label;

}
