<?php

namespace Drupal\wt_chunks\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Url;
use Drupal\wt_chunks\ChunkInterface;

/**
 * Defines the chunk entity class.
 *
 * @ContentEntityType(
 *   id = "chunk",
 *   label = @Translation("Chunk"),
 *   label_collection = @Translation("Chunks"),
 *   bundle_label = @Translation("Chunk type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\wt_chunks\ChunkListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "add" = "Drupal\wt_chunks\Form\ChunkForm",
 *       "edit" = "Drupal\wt_chunks\Form\ChunkForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   base_table = "chunk",
 *   data_table = "chunk_field_data",
 *   translatable = TRUE,
 *   admin_permission = "administer chunk types",
 *   entity_keys = {
 *     "id" = "id",
 *     "langcode" = "langcode",
 *     "bundle" = "bundle",
 *     "label" = "id",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "add-form" = "/admin/content/chunk/add/{chunk_type}",
 *     "add-page" = "/admin/content/chunk/add",
 *     "edit-form" = "/admin/content/chunk/{chunk}/edit",
 *     "delete-form" = "/admin/content/chunk/{chunk}/delete",
 *     "collection" = "/admin/content/chunk"
 *   },
 *   bundle_entity_type = "chunk_type",
 *   field_ui_base_route = "entity.chunk_type.edit_form"
 * )
 */
class Chunk extends ContentEntityBase implements ChunkInterface {

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    $fields = parent::baseFieldDefinitions($entity_type);

    return $fields;
  }

  public function label() {
    if ($this->hasField('field_title')) {
      return $this->get('field_title')->first()->getString();
    }

    $remote = [];
    if ($this->hasField('remote_datasource') || $this->hasField('remote_id')) {
      if ($this->hasField('remote_datasource')) {
        $remote[] = $this->get('remote_datasource')->first()->getString();
      }
      if ($this->hasField('remote_id')) {
        $remote[] = $this->get('remote_id')->first()->getString();
      }
      $remote = join(' | ', array_filter($remote));
    }

    return $remote ?: parent::label();
  }

  public function toUrl($rel = 'canonical', array $options = []) {
    if ($rel == 'canonical') {
      return Url::fromUri('route:<nolink>')->setOptions($options);
    }
    else {
      return parent::toUrl($rel, $options);
    }
  }
}
