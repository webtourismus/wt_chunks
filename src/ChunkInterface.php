<?php

namespace Drupal\wt_chunks;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface defining a chunk entity type.
 */
interface ChunkInterface extends ContentEntityInterface {

}
