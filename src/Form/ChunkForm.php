<?php

namespace Drupal\wt_chunks\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the chunk entity edit forms.
 */
class ChunkForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {

    $entity = $this->getEntity();
    $result = $entity->save();
    $link = $entity->toLink($this->t('View'))->toRenderable();

    $message_arguments = ['%label' => $this->entity->label()];
    $logger_arguments = $message_arguments + ['link' => render($link)];

    if ($result == SAVED_NEW) {
      $this->messenger()->addStatus($this->t('New chunk %label has been created.', $message_arguments));
      $this->logger('wt_chunks')->notice('Created new chunk %label', $logger_arguments);
    }
    else {
      $this->messenger()->addStatus($this->t('The chunk %label has been updated.', $message_arguments));
      $this->logger('wt_chunks')->notice('Updated new chunk %label.', $logger_arguments);
    }

    $form_state->setRedirect('entity.chunk.edit-form', ['chunk' => $entity->id()]);
  }

}
